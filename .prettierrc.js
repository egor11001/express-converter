module.exports = {
  ...require('gts/.prettierrc.json'),
  bracketSpacing: true,
  printWidth: 140,
};
