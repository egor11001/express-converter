import dotenv from 'dotenv';
import dotenvExpand from 'dotenv-expand';
import Joi from 'joi';

const env = dotenv.config();
dotenvExpand.expand(env);

const processEnvValidate = (env: NodeJS.ProcessEnv) => {
  const schema = Joi.object({
    PASSPORT_SECRET_KEY: Joi.string().min(3).max(30).required(),

    GOOGLE_CLIENT_ID: Joi.string().min(3).max(120).required(),
    GOOGLE_CLIENT_SECRET: Joi.string().min(3).max(120).required(),
    GOOGLE_CALLBACK: Joi.string().min(3).max(120).required(),

    PORT: Joi.number().required(),
    TZ: Joi.string().required(),
  }).unknown(true);

  const result = schema.validate(env);

  if (result.error) {
    throw new SyntaxError(`.env -> ${result.error.message}`);
  }
};

processEnvValidate(process.env);
