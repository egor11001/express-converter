import path from 'node:path';

const folder = 'src/db';

const db_paths = {
  USERS_PATH: path.join(folder, 'users.json'),
  TOKENS_PATH: path.join(folder, 'tokens.json'),
  FILES_PATH: path.join(folder, 'files.json'),
  UNAUTH_PATH: path.join(folder, 'unauth.json'),
};

export { db_paths };
