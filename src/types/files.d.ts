export interface FilesDb {
  files: FileDb[];
}

export interface FileDb {
  ready: boolean;
  status: number;
  user_id: string;
  id: string;
  type: string;
}
