import passport from 'passport';
import fs from 'fs';

import { Strategy as GoogleStrategy } from 'passport-google-oauth20';
import { Strategy as CustomStrategy } from 'passport-custom';

import { UserDb, UsersDb } from '../../types/users';
import { db_paths } from '../../db/db.paths';

passport.serializeUser((user, done) => {
  done(null, (user as UserDb)?.id);
});

passport.deserializeUser(async (id, done) => {
  const db: UsersDb = await JSON.parse(fs.readFileSync(db_paths.USERS_PATH!, 'utf8'));
  const user = db.users.find(item => item.id === id);

  return done(null, user);
});

passport.use(
  new GoogleStrategy(
    {
      clientID: process.env.GOOGLE_CLIENT_ID || '',
      clientSecret: process.env.GOOGLE_CLIENT_SECRET || '',
      callbackURL: process.env.GOOGLE_CALLBACK,
    },
    async (accessToken, refreshToken, profile, cb) => {
      const db: UsersDb = await JSON.parse(fs.readFileSync(db_paths.USERS_PATH!, 'utf8'));

      let user = db.users.find(item => item.id === profile.id);

      if (!user) {
        user = {
          id: profile.id,
          username: profile.username || profile.displayName,
          password: profile.displayName,
          email: profile.emails?.find(item => item.verified)?.value || null,
          source: profile.provider,
          uploaded: [],
          premium: 0,
        };

        db.users.push(user);

        fs.writeFileSync(db_paths.USERS_PATH!, JSON.stringify(db));
      }

      return cb(null, user);
    }
  )
);

passport.use(
  new CustomStrategy(async (req, done) => {
    const id = req.body.id;
    const db: UsersDb = await JSON.parse(fs.readFileSync(db_paths.USERS_PATH!, 'utf8'));
    const user = db.users.find(item => item.id === id);

    done(null, user);
  })
);

export { passport };
