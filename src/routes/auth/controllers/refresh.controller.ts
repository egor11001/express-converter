import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import fs from 'fs';

import { db_paths } from '../../../db/db.paths';

export const RefreshController = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { token } = req.body;

    const decoded = jwt.verify(token, process.env.JWT_SECRET_KEY!);

    if (!decoded) {
      throw Error;
    }

    const db = await JSON.parse(fs.readFileSync(db_paths.TOKENS_PATH!, 'utf8'));
    req.body.id = db[token];

    return next();
  } catch (err: any) {
    return res.json({ data: {}, err: err.message });
  }
};
