import { UploadedFile } from 'express-fileupload';
import sharp, { FormatEnum } from 'sharp';
import path from 'path';
import fs from 'fs';

import { FILE_DIR } from './constants';
import { FilesDb } from '../../types/files';
import { db_paths } from '../../db/db.paths';

interface ConvertFn {
  file: UploadedFile;
  format: keyof FormatEnum;
  quality?: number;
}

interface FileWorker {
  token: string;
  file: UploadedFile;
  format: keyof FormatEnum;
  quality?: number;
}

const convertFn = async ({ file, format, quality = 100 }: ConvertFn) => {
  try {
    await sharp(file.data)
      .toFormat(format, { quality: quality })
      .toFile(path.resolve(FILE_DIR, `${file.md5}.${format}`));
  } catch (err: any) {
    console.log(err.message);
  }
};

export const fileWorker = async ({ file, format, quality, token }: FileWorker) => {
  const db: FilesDb = await JSON.parse(fs.readFileSync(db_paths.FILES_PATH!, 'utf8'));
  let dbFileIndex: number | undefined;

  try {
    //get file index in db
    db.files.find((item, index) => {
      if (item.id === file.md5 && item.user_id === token) {
        dbFileIndex = index;
        return;
      }
    });

    if (dbFileIndex !== undefined) {
      const dbFile = db.files[dbFileIndex];
      dbFile.status = 50;

      db.files[dbFileIndex] = dbFile;
      fs.writeFileSync(db_paths.FILES_PATH!, JSON.stringify(db));

      await convertFn({ file, format, quality });

      dbFile.status = 100;
      dbFile.ready = true;

      db.files[dbFileIndex] = dbFile;
      fs.writeFileSync(db_paths.FILES_PATH!, JSON.stringify(db));

      return true;
    }

    return false;
  } catch (err: any) {
    console.log(err.message);
    return false;
  }
};
