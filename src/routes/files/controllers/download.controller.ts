import { Request, Response } from 'express';
import fs from 'fs';

import path from 'path';

import { ERR_ACCESS, ERR_FILE_FOUND, ERR_FILE_OVERRIDE, FILE_DIR } from './../constants';
import { FilesDb } from './../../../types/files.d';
import { db_paths } from '../../../db/db.paths';
import { AuthRequest } from '../../../types/users';

export const DownloadController = async (req: Request, res: Response) => {
  const authReq = req as AuthRequest;
  const user = authReq.user;
  const { id } = req.params;
  const { authorization } = req.headers;
  const token = user ? user.id : authorization;

  try {
    if (!token) {
      throw new Error(ERR_ACCESS);
    }

    const db: FilesDb = await JSON.parse(fs.readFileSync(db_paths.FILES_PATH!, 'utf8'));

    const files = db.files.filter(item => item.id === id && item.user_id === token);

    if (files.length > 1) {
      db.files = db.files.filter(item => item.id !== id && item.user_id === token);
      fs.writeFileSync(db_paths.FILES_PATH!, JSON.stringify(db));
      throw new Error(ERR_FILE_OVERRIDE);
    }

    const file = files[0];

    if (!file) {
      throw new Error(ERR_FILE_FOUND);
    }

    if (!file.ready) {
      return res.json({ data: file });
    }

    return res.download(path.resolve(FILE_DIR, `${file.id}.${file.type}`));
  } catch (err: any) {
    return res.json({ data: {}, err: err.message });
  }
};
