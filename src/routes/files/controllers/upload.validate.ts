import Joi from 'joi';
import { Request, Response, NextFunction } from 'express';

import * as constants from '../constants';

const schema = Joi.object({
  format: Joi.string().valid('jpeg', 'png', 'webp', 'gif', 'avif', 'tiff', 'svg').required(),
  quality: Joi.number().min(10).max(100),
  files: Joi.array()
    .items(
      Joi.object({
        size: Joi.number().max(constants.MAX_FILE_SIZE).required(),
        mimetype: Joi.string()
          .valid('image/jpeg', 'image/png', 'image/webp', 'image/gif', 'image/avif', 'image/tiff', 'image/svg')
          .required(),
        name: Joi.string(),
        data: Joi.binary().required(),
        encoding: Joi.string(),
        md5: Joi.string().required(),
        tempFilePath: Joi.any(),
        truncated: Joi.any(),
        mv: Joi.any(),
      })
    )
    .required(),
});

export const UploadValidate = async (req: Request, res: Response, next: NextFunction) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars, prefer-const
  let { format, quality } = req.body;
  let files = req.files?.files;

  quality = Number(quality);
  if (files && !Array.isArray(files)) {
    files = [files];
  }

  const { error } = schema.validate({ format, quality, files });

  if (error?.message) {
    return res.json({ data: {}, err: error.message });
  }
  return next();
};
