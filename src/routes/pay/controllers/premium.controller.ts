import { Request, Response } from 'express';
import fs from 'fs';

import { UsersDb, AuthRequest } from '../../../types/users';
import { db_paths } from '../../../db/db.paths';

export const PremiumController = async (req: Request, res: Response) => {
  const { plan } = req.body;
  const authReq = req as AuthRequest;
  const { id } = authReq.user!;
  let premiumUploads;

  try {
    if (Number(plan) !== 20) {
      throw new Error('Uncorrect plan');
    }

    const db: UsersDb = await JSON.parse(fs.readFileSync(db_paths.USERS_PATH!, 'utf8'));

    for (const userDb of db.users) {
      if (userDb.id === id) {
        userDb.premium += 200;
        premiumUploads = userDb.premium;
        break;
      }
    }

    fs.writeFileSync(db_paths.USERS_PATH!, JSON.stringify(db));

    return res.json({ data: { premiumUploads } });
  } catch (err: any) {
    return res.json({ data: {}, err: err.message });
  }
};
