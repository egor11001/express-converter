import Joi from 'joi';
import { Request, Response, NextFunction } from 'express';

const schema = Joi.object({
  plan: Joi.string(),
});

export const PremiumValidate = async (req: Request, res: Response, next: NextFunction) => {
  const { plan } = req.body;

  const { error } = schema.validate({ plan });

  if (error?.message) {
    return res.json({ data: {}, err: error.message });
  }
  return next();
};
